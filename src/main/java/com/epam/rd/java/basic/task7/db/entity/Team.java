package com.epam.rd.java.basic.task7.db.entity;

import com.epam.rd.java.basic.task7.db.DBManager;

import java.sql.Connection;
import java.util.Objects;

public class Team {

	private int id;
	private String name;
	private String previousName;
	private static DBManager dbm = DBManager.getInstance();
	private static Connection connection = DBManager.connection;
	private static final String INSERT_USER = "INSERT INTO teams VALUES (DEFAULT, ?)";
	private static final String GET_ID = "SELECT id FROM teams WHERE (name = (?))";
	private static final String UPDATE_TEAM = "UPDATE teams SET name=(?) WHERE name=(?)";

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		team.setPreviousName(name);
		return team;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
//		try {
//			int team_id = dbm.getTeam(getName()).getId();
//			PreparedStatement statement = connection.prepareStatement(UPDATE_TEAM);
//			statement.setString(1,name);
//			statement.setString(2,this.name);
//			statement.executeUpdate();
//		} catch (DBException | SQLException e) {
//			e.printStackTrace();
//		}
		this.previousName = this.name;
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		return this.name.equals(((Team) o).getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getName());
	}

	@Override
	public String toString() {
		return this.name;
	}

	public String getPreviousName() {
		return previousName;
	}

	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}
}
