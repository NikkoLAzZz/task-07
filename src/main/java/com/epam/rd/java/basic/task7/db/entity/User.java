package com.epam.rd.java.basic.task7.db.entity;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Objects;

public class User {

	private int id;
	private String login;
	private static final DBManager dbm = DBManager.getInstance();
	private static Connection connection = DBManager.connection;

	public static User createUser(String login) {
		User user = new User();
		user.setLogin(login);
		return user;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public boolean equals(Object o) {
		return this.login.equals(((User) o).getLogin());
	}

	@Override
	public String toString() {
		return this.login;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId(), getLogin());
	}
}
