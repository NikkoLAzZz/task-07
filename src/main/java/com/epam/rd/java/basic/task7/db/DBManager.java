package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final DBManager instance = new DBManager();
	public static final Connection connection = getInstance().getConnection();

	private static final String WORK_BD = "jdbc:derby:memory:testdb;create=true";

	private static final String SELECT_ALL_USERS = "SELECT * FROM users";
	private static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";

	private static final String SELECT_ALL_TEAMS = "SELECT * FROM teams";
	private static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";

	private static final String SET_TEAM_FOR_USER = "INSERT INTO users_teams VALUES(?, ?)";
	private static final String SELECT_ID_OF_TEAMS = "SELECT * FROM users_teams WHERE (user_id = (?))";
	private static final String SELECT_NAME_OF_TEAM = "SELECT name FROM teams WHERE (id = (?))";

	private static final String GET_USER = "SELECT * FROM users WHERE (login = (?))";
	private static final String GET_TEAM = "SELECT * FROM teams WHERE (name = (?))";

	private static final String DELETE_TEAM = "DELETE FROM teams WHERE (id = (?))";
	private static final String UPDATE_TEAM = "UPDATE teams SET name=(?) WHERE name=(?)";

	private static final String CHECK = "SELECT * from users_teams WHERE (user_id = (?) AND team_id = (?))";
	private DBManager() {
	}



	public static synchronized DBManager getInstance() {
		return instance;
	}

	private Connection getConnection(){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(WORK_BD);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public List<User> findAllUsers() throws DBException {
		List<User> list = null;
		User temp;
		try(Statement statement = connection.createStatement()) {
			ResultSet set = statement.executeQuery(SELECT_ALL_USERS);
			list = new ArrayList<>();
			while(set.next()){
				temp = new User();
				temp.setLogin(set.getString("login"));
				temp.setId(set.getInt("id"));
				list.add(temp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public boolean insertUser(User user) throws DBException {
		try(PreparedStatement statement = connection.prepareStatement(INSERT_USER)) {
			statement.setString(1,user.getLogin());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		return false;
	}

	public User getUser(String login) throws DBException {
		try(PreparedStatement statement = connection.prepareStatement(GET_USER)) {
			statement.setString(1,login);
			ResultSet set = statement.executeQuery();
			User user = new User();
			while(set.next()){
				user.setLogin(set.getString("login"));
				user.setId(set.getInt("id"));
			}
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Team getTeam(String name) throws DBException {
		try(PreparedStatement statement = connection.prepareStatement(GET_TEAM)) {
			statement.setString(1,name);
			ResultSet set = statement.executeQuery();
			Team team = new Team();
			while(set.next()){
				team.setName(set.getString("name"));
				team.setId(set.getInt("id"));
			}
			return team;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = null;
		Team temp;
		try(ResultSet set = connection.createStatement().executeQuery(SELECT_ALL_TEAMS)) {
			teams = new ArrayList<>();
			while (set.next()){
				temp = new Team();
				temp.setName(set.getString("name"));
				temp.setId(set.getInt("id"));
				teams.add(temp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(PreparedStatement statement = connection.prepareStatement(INSERT_TEAM)) {
			statement.setString(1, team.getName());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		System.out.println(user);
		for (Team team : teams) {
			System.out.print(team + " ");
		}
		System.out.println();
		try(PreparedStatement insert_users_teams = connection.prepareStatement(SET_TEAM_FOR_USER)) {
			int user_id = getUser(user.getLogin()).getId();
			StringBuilder cmd = new StringBuilder(SET_TEAM_FOR_USER);
			for (Team team : teams){
				int team_id = getTeam(team.getName()).getId();
				PreparedStatement check = connection.prepareStatement(CHECK);
				check.setInt(1,user_id);
				check.setInt(2,team_id);
				ResultSet set = check.executeQuery();
				Integer i = null;
				while (set.next()){
					i = set.getInt("team_id");
				}
				if (i != null){
					throw new SQLException();
				}
			}
			for (Team team : teams){
				insert_users_teams.setInt(1,user_id);
				int team_id = getTeam(team.getName()).getId();
				insert_users_teams.setInt(2,team_id);
				insert_users_teams.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = null;
		Team temp = null;
		try(PreparedStatement selectByUserIDFromUT = connection.prepareStatement(SELECT_ID_OF_TEAMS);
			PreparedStatement selectByNameFromT = connection.prepareStatement(SELECT_NAME_OF_TEAM))
		{
			int user_id = getUser(user.getLogin()).getId();
			selectByUserIDFromUT.setInt(1, user_id);
			ResultSet team_ids_set = selectByUserIDFromUT.executeQuery();
			teams = new ArrayList<>();
			int id_team;
			while (team_ids_set.next()){
				temp = new Team();
				id_team = team_ids_set.getInt("team_id");
				temp.setId(id_team);

				selectByNameFromT.setInt(1, id_team);
				ResultSet res = selectByNameFromT.executeQuery();
				while (res.next()){
					temp.setName(res.getString("name"));
					teams.add(temp);
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try {
			PreparedStatement delete = connection.prepareStatement(DELETE_TEAM);
			int team_id = getTeam(team.getName()).getId();
			delete.setInt(1,team_id);
			delete.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			int team_id = getTeam(team.getPreviousName()).getId();
			PreparedStatement statement = connection.prepareStatement(UPDATE_TEAM);
			statement.setString(1,team.getName());
			statement.setString(2,team.getPreviousName());
			statement.executeUpdate();
			return true;
		} catch (DBException | SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
